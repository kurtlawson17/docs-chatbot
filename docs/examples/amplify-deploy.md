---
id: setup-production-amplify
title: Setting up the Chatbot PWA with Amplify
sidebar_label: Deploy the Chatbot PWA with Amplify
---

The Chatbot PWA (Progressive Web App) must be configured through the Amplify CLI (Command Line Interface).

## Prerequisites

1. Ensure that an AWS (Amazon Web Services) account to deploy the PWA on is created
2. Ensure that an AWS Identity and Access Management (IAM) profile is created
3. Ensure that the AWS CLI tool is installed
For more information about platform specific procedures, see the [Amazon documentation](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
4. Ensure that you set up your AWS credentials on CLI
    * **Note:** Open a new terminal, and run `aws configure`
5. Ensure that [AWS Amplify CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) tool is installed

## Chatbot PWA on Amazon Amplify Example

1. Run `amplify init`. You may accept all default values and configure if necessary
2. Run `amplify add hosting` to enable static web hosting for the app on Amazon S3
3. In the following files, change the BotName field to the EPConversationalBot on your AWS account:
    * `./ep-pwa-chatbot/src/aws-exports.js` - `aws_bots_config.name = "[Your Bot Name]"`
    * `./ep-pwa-chatbot/src/App.js` - `botName = "[Your Bot Name]"`
4. Run `amplify publish` to build your PWA, and invoke the push to Amazon S3

After completing the set up, you can access your PWA at the domain name provided in the console.

![Chatbot PWA](assets/chatbot-pwa.png)
