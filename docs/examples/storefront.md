---
id: setup-storefront
title: Setup the chatbot with the React PWA Reference Storefront
sidebar_label: Setup the chatbot with the React PWA Reference Storefront
---

The React PWA (Progressive Web Application) Reference Storefront is developed to integrate with the Reference Chatbot.
