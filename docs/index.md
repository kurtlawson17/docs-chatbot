---
id: index
title: Reference Elastic Path Conversational Interface Quickstart Guide
sidebar_label: Overview
---

This document provides guidelines to setup and configure the Reference AWS (Amazon Web Services) Lex Chatbot, and how to integrate the Reference Chatbot with the React PWA (Progressive Web App) Reference Storefront. However, this document is not a primer for JavaScript and is intended for professionals who are familiar with the following technologies:

* [Git](https://git-scm.com/downloads)
* [AWS Dynamo DB](https://aws.amazon.com/dynamodb/)
* [AWS Lambda Functions](https://aws.amazon.com/lambda/)
* [AWS Lex Models](https://aws.amazon.com/lex/)

## Related Resources

* [Chatbot](setup.html)
* [React PWA Reference Storefront Repository](https://github.com/elasticpath/react-pwa-reference-storefront/)

## Overview

The Elastic Path Conversational Interface is an AWS Lex-based model that allows flexible deployment to a variety of conversational interfaces, including Facebook Messenger and Slack. This model communicates with Elastic Paths RESTful API, Cortex API, to leverage the e-commerce capabilities provided by Elastic Path.

![Architecture Diagram](assets/architecture.png)

## Project Structure

* The Lambda Deployment Package lives under `./ep-lambda-function`

* The Lex Model lives under `./ep-lex-models`

* The PWA Chatbot component lives under `./ep-pwa-chatbot`

For more information about the Lex Model, visit the [Lex Developer Documentation](https://docs.aws.amazon.com/lex/latest/dg/import-export.html).

## Requirements

To install and customize the Reference Chatbot, you must have the following software:

* A valid Elastic Path development environment. For more information, see [The Starting Construction Guide](https://developers.elasticpath.com/commerce/construction-home)
* [Git](https://git-scm.com/downloads)
* [Node.js](https://nodejs.org/en/download/)
* [Amplify CLI](https://aws-amplify.github.io/docs/)
* A valid [Amazon Web Services (AWS) Account](https://us-west-2.console.aws.amazon.com/console/)
* A valid [Amazon Developer Account](https://developer.amazon.com/)
