---
id: chatbot-server
title: Setting up the Chatbot Server
sidebar_label: Chatbot server
---

> **Note:** The following instructions are for the [Reference Chatbot using Facebook Messenger](https://github.com/elasticpath/facebook-chat).
<!-- {blockquote:.is-warning} -->

## Environment File

The application requires some information that can be stored either in a environment file (`.env`) or as constants directly in the code.

Example of `.env` file:

```env
VERIFY_TOKEN='<Randomly_Generated_Token_To_Provide_To_Facebook>'
PAGE_ACCESS_TOKEN='<Token_Provided_By_Facebook>'

EP_SERVER='http://<Customer_demo_url>/cortex'
EP_SCOPE='<Customer_Store>'
EP_IMAGES='https://s3-us-west-2.amazonaws.com/<link_to_images>/'
```

Example of contstants:

```javascript
const VERIFY_TOKEN = '<Randomly_Generated_Token_To_Provide_To_Facebook>';
const PAGE_ACCESS_TOKEN='<Token_Provided_By_Facebook>';

const EP_SERVER='http://<Customer_demo_url>/cortex';
const EP_SCOPE='<Customer_Store>';
const EP_IMAGES='https://s3-us-west-2.amazonaws.com/<link_to_images>/';
```

## Running the application

1. Ensure you have read through the documentation [here](https://github.com/elasticpath/facebook-chat/blob/master/README.md)
2. Run the `cd chatbot` command
3. To install dependencies, run the `npm install` command
4. To run the application, run the `node app.js` command
5. Configure the `./src/ep.config.json` file as required for the environment.

    For more information, see the [Configuration Parameter Descriptions](#configuration-parameter-descriptions) section.

6. Run `npm start` to start the server in development mode. The following message is displayed:

    ![Running the application](assets/app-running.png)

7. Use `<Ctrl>-C` to stop the application

## Configuring the Facebook API

In this part, we are going to assume that you already created a Facebook business page. If you did not, please review the [appendices](#appendices) first.

1. Go to [Facebook for developer](https://developers.facebook.com) and log in with the page admin credentials.

    If you have multiple pages on this account, select the account to create the chatbot for..

2. On the sidebar, click the (+) button next to Products, and search for `Messenger`, (it should be one of the first 3 products)
3. Click on **Set Up** to configure Messenger

    `Messenger` appears in the sidebar.

4. In the Set Up page, you should see a block called `Token Generation`, select your Facebook page in this block.

    Facebook creates a random token. Copy the token, go to your environment file or constant, and populate the field `PAGE_ACCESS_TOKEN`.

5. At the top of the page, you should see `APP ID: <TOKEN>`. Copy `<TOKEN>` and head to your Facebook page
6. Click `Settings` on the top-right corner of the page
7. On the sidebar, click Messenger Platform and scroll down to `Subscribed Apps`, this should be empty for now
8. Just below this block, you can see `Link your App to Your Page`, in the field, paste the `<TOKEN>` you previously copied (i.e. `APP ID`) and hit `Link`

    This populates the block `Subscribed Apps`.

9. Head back to [Facebook for developer](https://developers.facebook.com)
10. On the sidebar, click Roles, this will open a dropdown menu, click Roles in this sub-menu
11. You can see `Administrators` with your Facebook account and `Testers` with none in this block
12. Click `Add Testers`, this will open a modal, search for your tester account and click submit
13. Open a new incognito page and head to Facebook. Log in as the tester
14. You should get a notification from your business page. Click accept to become a tester

The Facebook API is now configured and the tester is able to test the chatbot.

## Deploy the application

For the final configuration step, it is required first to deploy the application.

Go to `/etc/apache2/sites-available` and update `000-default.conf` to add this:

```conf
ProxyPass /chatbot http://127.0.0.1:<PORT>
ProxyPassReverse /chatbot http://127.0.0.1:<PORT>
```

> `<PORT>` is the port used by the node application.

Facebook will not accept not secure communication, so you need to install an SSL certificate from a valid CA. We are going to use Let’s Encrypt for this part.

1. Run the command `sudo add-apt-repository ppa:certbot/certbot`
2. Run `sudo apt-get update`
3. Run `sudo apt-get install python-certbot-apache`
4. Run `sudo certbot --apache -d <Customer>.epdemos.com`, where `<Customer>` is the subdomain that you are using
5. Complete the form from certbot

    > Ensure that `http` does not redirect to `https` by default. If it redirects, applications such as, Cortex, Commerce Manager, and Studio from port `80` to port `443` will cause issues making requests. For example, requests are redirected, causing the `HTTP` code to be `30X` instead of `20X`.

6. You should have a new file: `/etc/apache2/sites-available/000-default-le-ssl.conf`.

    This file contains the exact same thing as `000-default.conf` but for port `443`.

7. Run `npm install -g forever`
8. Head back to the Github repository of the chatbot and run `forever start app.js`.

    This runs the Node application as a background task and you can get control and leave the Virtual Machine (VM) without stopping the application.

9. Copy the content of the `VERIFY_TOKEN` constant or property

    If you did not create a token, generate one first. Use a 40 to 50 character long token containing digits and capitalized letters.

10. Head back to [Facebook for developer](https://developers.facebook.com)
11. Click `Add subscription` or `Edit subscription` and paste the token in the `Verify Token` field
12. In the callback URL field add `https://<Customer>.epdemos.com/chatbot/webhook` where `<Customer>` is the subdomain of your application
13. Click `Verify and Save`. If this doesn’t work, in the `Callback URL` field, there should be a red cross on the right, if you hover it you should see the cause of the error.
    * If you get a 404 error then it is likely that the deployment went wrong or that the application crashed
    * If you get a 403 error, then you have a configuration issue in your `.env` file or in your constants

    To debug these issues:

    * Run `forever stop app.js`
    * In the first method `app.get('/webhook', (req, res) => {...` add logs to check that the values of `token` (which should be your `VERIFY_TOKEN`) and `mode` (which should be `subscribe`)

### Building a docker image

The application contains a `Docker` folder that contains a `Dockerfile` and a Shell script. The `Dockerfile` uses a NodeJS image and contains multiple arguments that you can populate using the `--build-args` argument when building the image. These Docker arguments are used in the Shell script to replace the constants in the code, this way, the chatbot is easily deployable and doesn’t require to modify directly the code.

|  Parameter| Importance|Type|Description|
|--|--|--|--|
|`PAGE_ACCESS_TOKEN`| Required| String| The token that is provided by Facebook on (developers.facebook.com) for each application.|
|`VERIFY_TOKEN`| Required| String| The token that you have to generate and pass to Facebook when configuring the webhooks.|
|`SCOPE`|Required|String| Name of the store from which Cortex retrieves data.|
|`S3_URL`|Required|String| The URL that consists of the path to images hosted on an external CMS (Content Management System). Set this parameter to the complete URL of the images by replacing the `<link_to_images>` with the path of your images|
|`FIREBASE_API_KEY`|Required|String| Your firebase API key.|
|`FIREBASE_AUTH_DOMAIN`|Required|String| This is the unique domain provided by Firebase for your application.|
|`FIREBASE_APP_NAME`|Required|String| This is the name you use to register your application on Firebase.|
|`FIREBASE_DB_URL`|Required|String| This is the database URL that Firebase provides.|

## Features

In its current state. The chatbot is able to perform multiple actions:

* It contains regular actions that are triggered by clicking on buttons in the Messenger application:
    * Login to Cortex
    * Add an item to the cart
    * Add an item to the wishlist
    * Checkout
    * Move an item from the cart to the wishlist and vice-versa
* Contains 3 nlp features that use a limited vocabulary.
    * You can search items based on keywords, for example: `search for golf bag` will perform a search request with keywords `golf` and `bag`
   ![Chatbot Search](assets/chatbot_search_store.gif)
    * You can add items in your cart based on keywords and on quantity, for example: `add 3x golf bag` will perform a search based on `golf` and `bag` and will send a response that enables the user to add 3 of the found items in their cart
   ![Chatbot Add To Cart](assets/chatbot_add_to_cart.gif)
    * You can apply a promotion coupon, for example: `apply REDUCTION10` will apply the `REDUCTION10` coupon if it exists
* Ability to link a Facebook account to a Cortex account
    * This is done by authenticating on the login application that is included in the chatbot, this application exposes a form that that performs authentication requests to a Cortex server

## Making tests

Go to Facebook and log in as the tester (i.e. the App tester).
Head to the page to test and click `Send Message` on the top on the right.

Write anything in the chat box, and the chatbot should reply with:

> Welcome back Emma! You don’t have items in your shopping bag. How can I help you today?

Go to Studio and log as the tester, make sure to have all the required information set up.

Add items to your cart and go back to Facebook. Write anything in the chat box and you should see something like that:

![Chat with items in cart](assets/chat-with-items.png)

If you click `Not Sure` the bot will put the items in your wishlist.
If you click `Yes!` the bot will pass the order:

![Checkout](assets/checkout.png)

You can then click on the order to have a new component popping up:

![Order review](assets/order-review.png)

![Chatbot Order History](assets/chatbot_order_history.gif)

At this point the chatbot is up and running

## Appendices

To create a Facebook business page, log in as your page admin account.
In the sidebar, click Pages, just in the `Explore` block.
Click `Create Page` on the top right corner and click `Business or Brand`, add a page name and a category (Clothing, Brand, Computers...)
Click continue, if this is your first page, it will be asked to add a cover photo and a profile picture. Skip this part as it does not matter.
