---
id: index
title: Reference Chatbot Quick Start Guide
sidebar_label: Overview
---

> **Note:** The following instructions are for the [Reference Chatbot using Facebook Messenger](https://github.com/elasticpath/facebook-chat).
<!-- {blockquote:.is-warning} -->

This document provides guidelines to setup and configure the Reference Chatbot, and integrate it with the React PWA (Progressive Web App) Reference Storefront. However, this document is not a primer for JavaScript and is intended for professionals who are familiar with the following technologies:

- [Nodejs](https://nodejs.org/en/)

## Related Resources

- [Chatbot Server](chatbot-server.html)
- [Login Server](login-server.html)
- [React PWA Reference Storefront Repository](https://github.com/elasticpath/react-pwa-reference-storefront/)

## Overview

The Reference Chatbot is a flexible chatbot integrated with Facebook Messenger, which communicates with Elastic Path’s RESTful e-commerce API, Cortex API. Through the Cortex API, the chatbot uses the e-commerce capabilities provided by Elastic Path Commerce and interacts with data in a RESTful manner.

![Chatbot Intro](assets/chatbot_intro.gif)

## Setting up the Chatbot

### Prerequisites

Ensure that the following software are installed:

- [Git](https://git-scm.com/downloads)
- [Node.js](https://nodejs.org/en/download/)
- [Visual Studio Code](https://code.visualstudio.com/) with the following extensions:
    - [Debugger for Chrome](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome)
    - [ESLint extension](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- A valid Elastic Path development environment. For more information, see
[The Starting Construction Guide](https://developers.elasticpath.com/commerce/construction-home)

### Setting up a Development Environment

1. Clone or pull the `facebook-chat` repository to your directory
2. Run the `cd facebook-chat` command
3. Follow the instructions in [Setting up Chatbot server](chatbot-server.html)
4. Follow the instructions in [Setting up Login server](login-server.html)
5. (Optional) You may integrate the Reference Chatbot with the React PWA Reference Storefront available [here](https://github.com/elasticpath/react-pwa-reference-storefront/)
