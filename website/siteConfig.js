// See https://docusaurus.io/docs/site-config for all the possible site configuration options.

const projectRepo = "https://github.com/elasticpath/lex-chatbot";
const docRepo = "https://gitlab.com/elasticpath/reference-experience/docs-chatbot" ;
const args = process.argv.slice(2);
const siteUrl = args[0] ? "/" + args[0] + "/" : '/chatbot/';


const siteConfig = {
  title: 'Reference Chatbot', // Title for your website.
  tagline: 'Documentation for Reference Chatbot',
  url: 'https://documentation.elasticpath.com', // Your website URL
  baseUrl: siteUrl, // Base URL for your project */
  // For github.io type URLs, you would set the url and baseUrl like:
  //   url: 'https://facebook.github.io',
  //   baseUrl: '/test-site/',

  // Used for publishing and more
  projectName: 'docs-chatbot',
  organizationName: 'elasticpath',
  // For top-level user or org sites, the organization is still the same.
  // e.g., for the https://JoelMarcey.github.io site, it would be set like...
  //   organizationName: 'JoelMarcey'

  // For no header links in the top nav bar -> headerLinks: [],
  headerLinks: [
    { doc: 'index', label: 'Documentation' },
    { href: 'https://developers.elasticpath.com', label: 'Developer Center' },
    { href: projectRepo, label: 'Github', external:true }
  ],

  //Algolia search configuration
  algolia: {
    apiKey: '41c3c39624ad5fa69279fe2df25fae7a',
    indexName: 'elasticpath_chatbot',
    placeholder: 'Search docs',
  },

  /* path to images for header/footer */
  headerIcon: 'img/ep-logo-white.png',
  footerIcon: 'img/ep-logo-white.png',
  favicon: 'img/favicon/ep-logo.png',

  /* Colors for website */
  colors: {
    primaryColor: '#0033cc',
    secondaryColor: '#ea7317',
  },

  // This copyright info is used in /core/Footer.js and blog RSS/Atom feeds.
  copyright: `Copyright © ${new Date().getFullYear()} Elastic Path, Inc.`,

  highlight: {
    // Highlight.js theme to use for syntax highlighting in code blocks.
    theme: 'default',
  },

  // Add custom scripts here that would be placed in <script> tags.
  scripts: ['https://buttons.github.io/buttons.js'],

  // On page navigation for the current documentation page.
  onPageNav: 'separate',
  // No .html extensions for paths.
  // cleanUrl: true,

  // Open Graph and Twitter card images.
  ogImage: 'img/ep-192x192.png',
  twitterImage: 'img/ep-192x192.png',
  twitterUsername: 'elasticpath',

  // Show documentation's last contributor's name.
  // enableUpdateBy: true,

  // Show documentation's last update time.
  enableUpdateTime: true,
  editUrl: docRepo + '/edit/master/docs/',
  //Enable scrolling to top button
  scrollToTop: true,

  // You may provide arbitrary config keys to be used as needed by your
  // template. For example, if you need your repo's URL...
  //   repoUrl: 'https://github.com/facebook/test-site',
};

module.exports = siteConfig;
